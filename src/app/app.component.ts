import { Component } from "@angular/core";
import { NgModel } from "@angular/forms";
import { TimelineData } from "./interfaces/timelineData";
import { HolidaysService } from "./services/holidays.service";
@Component({
  selector: "app-root",
  templateUrl: "./app.component.html"
})
export class AppComponent {

  public year: number;
  public years: number[] = [];
  public timelineData: TimelineData;
  public errorMessage: string;

  constructor(private _holidaysService: HolidaysService) {}

  public ngOnInit(): void {
    //Carga el dropdown
    this.years = this._holidaysService.getYears();
  }

  public getHolidays() {
    this._holidaysService.getHolidays(this.year).subscribe(
      data => {
        this.timelineData = {
          year: this.year,
          data: data
        };
      },
      error => {
        this.errorMessage = "Hubo un error, por favor vuelva a intentar...";
      }
    );
  }
}
