import { Component, OnInit, Input } from "@angular/core";
import { Timeline, DataSet, IdType } from "vis";

@Component({
  selector: "app-timeline",
  templateUrl: "./timeline.component.html"
})
export class TimelineComponent implements OnInit {
  private timeline: Timeline;
  public loading: boolean;
  private item: object;
  private _timelineData;
  public informationMessage: string;
  private months: string[];

  constructor() {
    this.months = [
      "Enero",
      "Febrero",
      "Marzo",
      "Abril",
      "Mayo",
      "Junio",
      "Julio",
      "Agosto",
      "Septiembre",
      "Octubre",
      "Noviembre",
      "Diciembre"
    ];
  }

  //Se utiliza getter y setter para evitar el uso de ngOnChanges
  //por cuestiones de performance
  get timelineData(): string {
    return this._timelineData;
  }

  @Input()
  set timelineData(timelineData: string) {
    this._timelineData = timelineData;
    if (this._timelineData !== undefined) {
      this.generateTimeline(this._timelineData.year, this._timelineData.data);
    }
  }

  ngOnInit() {}

  //Método para generar el timeline
  private generateTimeline(year: number, data: any) {
    this.informationMessage = "";

    this.destroyTimelineIfExists();

    this.loading = true;
    //Ubica el contenedor
    let container = document.getElementById("timeline");
    //Agrega los items
    let items = new DataSet(this.setTimelineItems(year, data));
    //Setea las opciones
    let options = { locale: "es", zoomMin: 1000000000, showTooltips: true };
    //Instancia el Timeline
    this.timeline = new Timeline(container, items, options);
    //Declara el zoom inicial
    this.timeline.setWindow(new Date(year, 0, 1), new Date(year, 0, 20));
    //Oculta el spinner y muestra mensaje
    setTimeout(() => {
      this.loading = false;
      this.informationMessage =
        "Podés cambiar el zoom con la rueda del mouse o arrastrar la linea para ver el resto del año.";
    }, 1000);
  }

  private destroyTimelineIfExists() {
    if (this.timeline !== undefined) {
      this.timeline.destroy();
    }
  }

  private setTimelineItems(year: number, data: any) {
    let datasetArray = [];
    let message;

    for (let i = 0; i < data.length; i++) {
      if (data[i].tipo == "trasladable") {
        message = this.setTooltipMessage(
          data[i].dia,
          data[i].mes,
          data[i].original
        );
      } else {
        message = this.setTooltipMessage(data[i].dia, data[i].mes);
      }

      this.item = {
        id: i,
        content: data[i].motivo,
        title: message,
        start: year + "-" + data[i].mes + "-" + data[i].dia
      };

      datasetArray.push(this.item);
    }

    return datasetArray;
  }

  private setTooltipMessage(
    finalDay: number,
    finalMonth: number,
    original?: string
  ) {
    let message = "";

    //Si es un feriado trasladable se determina el tipo de mensaje
    //dependiendo si se terminó cambiando de día o no.
    if (original !== undefined) {
      let originalDate = original.split("-");

      if (Number(originalDate[0]) == finalDay) {
        message = finalDay + " del " + this.months[finalMonth - 1];
      } else {
        message =
          Number(originalDate[0]) +
          " de " +
          this.months[Number(originalDate[1]) - 1] +
          " se pasa al " +
          finalDay +
          " de " +
          this.months[finalMonth - 1];
      }
    } else {
      //Si el feriado no es trasladable el tooltip muestra la fecha
      message = finalDay + " de " + this.months[finalMonth - 1];
    }

    return message;
  }
}
