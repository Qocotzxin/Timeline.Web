import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AppComponent } from "./app.component";
import { FormsModule } from "@angular/forms";
import { BaseService } from "./services/base.service";
import { HolidaysService } from "./services/holidays.service";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { TimelineComponent } from './components/timeline/timeline.component';

@NgModule({
  declarations: [AppComponent, TimelineComponent],
  imports: [
    BrowserModule,
    FormsModule,
    CommonModule,
    HttpClientModule
  ],
  providers: [BaseService, HolidaysService],
  bootstrap: [AppComponent]
})
export class AppModule {}
