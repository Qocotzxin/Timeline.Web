import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { BaseService } from "./base.service";
import { SystemConfugration } from "../configuration/system-confugration";

@Injectable()
export class HolidaysService extends BaseService {
  private url: string;

  constructor(private _http: HttpClient) {
    super(_http);
  }

  getHolidays(year: number) {
    this.url = SystemConfugration.API_URL + year;
    return super.get(this.url);
  }

  getYears() {
    //Carga un array con años que incluye 5 años antes al actual y 1 más.
    //Esto es porque la API contiene datos sólo hasta al año siguiente al actual.
    let lastYearToSearch = Number(new Date().getFullYear()) + 1;
    let years = [];

    for (let i = 0; i < 7; i++) {
      years.push(lastYearToSearch - i);
    }

    return years;
  }
}
