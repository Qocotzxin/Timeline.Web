//Base Service es un servicio base realizado para 
//extender funciones http a todos lose servicios que lo requieran

import { Injectable } from "@angular/core";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { catchError } from "rxjs/operators";
import { Observable, ObservableInput } from "rxjs/Observable";
import "rxjs/add/observable/throw";
@Injectable()
export class BaseService {
  constructor(private http: HttpClient) {}

  protected get(url: string) {
    return this.http.get(url).pipe(catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse): ObservableInput<any> {
    throw Observable.throw(error.status);
  }
}
