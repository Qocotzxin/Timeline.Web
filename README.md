# Timeline

Este proyecto es de acceso público, y se encuentra dentro del repositorio de Cristian Etchebarne (@Qocotzxin).

Incluye el test realizado para Avenida.com, utilizando las siguientes teconlogías:

- Angular v5.2
- Angular-CLI 1.7.3.
- Node v8.1
- NPM v5.6
- Vis Js v4.2
- Font Awsome v5
- Bootstrap v4.1
- Google Fonts
- Moment JS v2.21
## Pasos para utilizar el proyecto 

- git clone https://gitlab.com/Qocotzxin/Timeline.Web
- cd Timeline.Web
- npm install
- ng serve -o

